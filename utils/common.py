import os


def is_rpi():
    return 'arm' in os.uname()[4]


def get_serial():
    """
    Get serial number of the device
    :return:
    """
    if is_rpi():
        cpuserial = "0000000000000000"
        try:
            f = open('/proc/cpuinfo', 'r')
            for line in f:
                if line[0:6] == 'Serial':
                    cpuserial = line[10:26].lstrip('0')
            f.close()
        except:
            cpuserial = "ERROR000000000"
        return cpuserial
    else:
        return '12345678'
