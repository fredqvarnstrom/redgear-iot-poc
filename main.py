import threading
import logging.config
import os
import time
from sensors.bme280 import read_bme280
from sensors.tsl2591_read import read_light
from sensors.audio_analyser import AudioAnalyser
from utils.config_util import get_config
from orm import Enviro, Alarm, Movement, Buttons
import RPi.GPIO as GPIO

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

logging.config.fileConfig(os.path.join(cur_dir, 'logging.ini'))
logger = logging.getLogger('RedGearIoT')

GPIO.setmode(GPIO.BCM)

pir_pin = int(get_config(section='pir', option='pir_pin', default=4))
btn_pin = int(get_config(section='button', option='button_pin', default=20))

GPIO.setup(pir_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(btn_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

TRIG = int(get_config(section='ultrasonic', option='trig_pin', default=23))
ECHO = int(get_config(section='ultrasonic', option='echo_pin', default=24))

GPIO.setup(TRIG, GPIO.OUT)                  # Set pin as GPIO out
GPIO.setup(ECHO, GPIO.IN)                   # Set pin as GPIO in


led_pin = int(get_config(section='LED', option='led_pin', default=21))
GPIO.setup(led_pin, GPIO.OUT)


timeout = 5


class RedGearIoT(threading.Thread):

    b_stop = threading.Event()

    def __init__(self, *args, **kwargs):
        super(RedGearIoT, self).__init__(*args, **kwargs)

        self.interval = int(get_config(section='general', option='interval', default=1))

        self.s_sound = AudioAnalyser()

        if not Enviro.table_exists():
            logger.warning('Table `Enviro` does not exist, creating...')
            Enviro.create_table()

        if not Alarm.table_exists():
            logger.warning('Table `Movement` does not exist, creating...')
            Alarm.create_table()

        if not Movement.table_exists():
            logger.warning('Table `Movement` does not exist, creating...')
            Movement.create_table()

    def run(self):
        while not self.b_stop.isSet():
            s_time = time.time()
            try:
                distance = read_distance()
                decibel = self.s_sound.get_audio_level()
                if decibel is None:
                    logger.error('Failed to get sound level, maybe MIC is disconnected?')
                    decibel = 0
                else:
                    decibel = int(decibel)
                temperature, humidity, pressure = read_bme280()
                lux = read_light()
                Enviro.create(temp=temperature, humid=humidity, light=lux, sound=decibel)
                Movement.create(dist=distance)
                elapsed = time.time() - s_time
                logger.debug('Distance: {}cm, Temp: {}C, Humidity: {}%, Pressure: {}hPa, Light: {}lux, Sound: {} dB  '
                             'Elapsed: {}'.format(distance, temperature, humidity, pressure, lux, decibel, elapsed))
            except Exception as e:
                logger.exception('Error: {}'.format(e))

            elapsed = time.time() - s_time
            time.sleep(max(0, self.interval - elapsed))

    def stop(self):
        self.b_stop.set()


def on_button_pressed(channel):
    state = GPIO.input(btn_pin)
    if state:
        logger.info('Button is released!')
    else:
        logger.info('Button is pressed!')
        GPIO.output(led_pin, True)
        if not Buttons.table_exists():
            Buttons.create_table()
        Buttons.create(pressed=True)
    GPIO.output(led_pin, not state)


def on_movement_detected(channel):
    logger.info('Motion is detected!')
    if not Movement.table_exists():
        Movement.create_table()
    Movement.create(pir=True)


def read_distance():
    GPIO.output(TRIG, True)                  # Set TRIG as HIGH
    time.sleep(0.00001)                      # Delay of 0.00001 seconds
    GPIO.output(TRIG, False)                 # Set TRIG as LOW

    s_time = time.time()
    pulse_start = time.time()
    while GPIO.input(ECHO) == 0:               # Check whether the ECHO is LOW
        pulse_start = time.time()              # Saves the last known time of LOW pulse
        if time.time() - s_time > timeout:
            print 'ECHO is always LOW!, skipping'
            return 0

    s_time = time.time()
    pulse_end = time.time()
    while GPIO.input(ECHO) == 1:               # Check whether the ECHO is HIGH
        pulse_end = time.time()                # Saves the last known time of HIGH pulse
        if time.time() - s_time > timeout:
            print 'ECHO is always HIGH!, skipping'
            return 0

    pulse_duration = pulse_end - pulse_start    # Get pulse duration to a variable

    distance = pulse_duration * 17150        # Multiply pulse duration by 17150 to get distance
    distance = round(distance, 2)            # Round to two decimal points

    if distance > 500:
        return 0
    else:
        return distance - .5


if __name__ == '__main__':

    logger.debug('=============== Starting RedGear IoT Node ===============')

    delay_time = 20
    logger.debug('Delay for {} sec...'.format(delay_time))
    time.sleep(delay_time)

    GPIO.output(TRIG, False)  # Set TRIG as LOW
    print "Waiting For Sensor To Settle"
    time.sleep(.5)  # Delay of 2 seconds

    GPIO.add_event_detect(btn_pin, GPIO.BOTH, callback=on_button_pressed, bouncetime=200)
    GPIO.add_event_detect(pir_pin, GPIO.RISING, callback=on_movement_detected, bouncetime=200)
    rd = RedGearIoT()
    rd.start()
