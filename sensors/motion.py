import time

import datetime
from gpiozero import MotionSensor
from utils.config_util import get_config

pir_pin = int(get_config(section='pir', option='pir_pin', default=4))

s_pir = MotionSensor(pin=pir_pin)


def on_movement_detected():
    print datetime.datetime.now(), '  Motion is detected!'


s_pir.when_activated = on_movement_detected


while True:
    time.sleep(1)
