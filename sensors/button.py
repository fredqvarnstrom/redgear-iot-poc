import time
from gpiozero import Button

from utils.config_util import get_config

btn_pin = int(get_config(section='button', option='button_pin', default=20))


def on_button_pressed():
    print ('Button is pressed!')


s_btn = Button(pin=btn_pin, bounce_time=.01)
s_btn.when_activated = on_button_pressed


while True:
    time.sleep(100)
