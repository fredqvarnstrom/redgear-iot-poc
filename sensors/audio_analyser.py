import numpy
import pyaudio
import analyse
import time

# Initialize PyAudio
pyaud = pyaudio.PyAudio()


class AudioAnalyser:

    device = None
    timeout = 5
    stream = None

    def __init__(self, timeout=5):
        self.timeout = timeout
        self.detect_mic()

    def get_audio_level(self):
        if self.stream is None:
            print 'Could not find USB microphone, please check connectivity'
            return None

        s_time = time.time()
        while True:
            try:
                rawsamps = self.stream.read(1024)
                samps = numpy.fromstring(rawsamps, dtype=numpy.int16)
                return analyse.loudness(samps)
            except IOError as e:
                # print "#" * 10
                # traceback.print_exc()
                if time.time() - s_time > self.timeout:
                    print 'Error, cannot read audio stream, please check USB microphone.'
                    return None
                else:
                    time.sleep(.1)

    def detect_mic(self):
        for i in range(pyaud.get_device_count()):
            dev = pyaud.get_device_info_by_index(i)
            if 'USB' in dev['name']:
                print 'USB Microphone found: ', dev
                self.device = dev
                self.stream = pyaud.open(format=pyaudio.paInt16, channels=dev['maxInputChannels'],
                                         rate=int(dev['defaultSampleRate']),
                                         frames_per_buffer=int(dev['defaultSampleRate']),
                                         input_device_index=dev['index'], input=True)


if __name__ == '__main__':
    ctrl = AudioAnalyser()

    while True:
        print ctrl.get_audio_level()
        time.sleep(1)
