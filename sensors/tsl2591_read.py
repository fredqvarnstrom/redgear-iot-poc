import tsl2591

tsl = tsl2591.Tsl2591()


def read_light():
    """
    Read lux value from TSL2591 module
    :return:
    """
    try:
        full, ir = tsl.get_full_luminosity()    # read raw values (full spectrum and ir spectrum)
        lux = tsl.calculate_lux(full, ir)       # convert raw values to lux
        return lux
    except Exception as e:
        print 'Failed to get data from TSL2591 - {}'.format(e)


if __name__ == '__main__':

    import time

    while True:
        print('{} Lux'.format(read_light()))
        time.sleep(1)
