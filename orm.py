import datetime
from peewee import *
from utils.common import get_serial
from utils.config_util import get_config


host = get_config(section='db', option='host', default='172.24.1.1')
database = get_config(section='db', option='database', default='rgsneuron')
port = int(get_config(section='db', option='port', default=3306))
username = get_config(section='db', option='username', default='rguser')
password = get_config(section='db', option='password', default='Mj$AU$5t')


db = MySQLDatabase(host=host, port=port, user=username, passwd=password, database=database)
db.connect()


class Enviro(Model):
    """
    ORM class for the environmental sensor data from Edge Device
    """
    temp = IntegerField(null=True)
    humid = IntegerField(null=True)
    light = IntegerField(null=True)
    sound = IntegerField(null=True)
    update = DateTimeField(default=datetime.datetime.now)
    source = TextField(default=get_serial())

    class Meta:
        database = db


class Alarm(Model):
    dist = IntegerField(null=True)
    update = DateTimeField(default=datetime.datetime.now)
    source = TextField(default=get_serial())

    class Meta:
        database = db


class Movement(Model):
    pir = BooleanField(null=True)
    update = DateTimeField(default=datetime.datetime.now)
    source = TextField(default=get_serial())

    class Meta:
        database = db


class Buttons(Model):
    pressed = BooleanField(null=True)
    update = DateTimeField(default=datetime.datetime.now)
    source = TextField(default=get_serial())

    class Meta:
        database = db


if __name__ == '__main__':
    for item in Enviro.select():
        print 'Temp: {}, Humidity: {}, Light: {}, Sound: {}, Update: {}'.format(
            item.temp, item.humid, item.light, item.sound, item.update)
